core = 7.x

api = 2
projects[drupal][version] = "7.x"

; Modules
projects[abbrfilter][subdir] = "contrib"
projects[adaptive_image][subdir] = "contrib"
projects[advanced_help][subdir] = "contrib"
projects[admin_menu][subdir] = "contrib"
projects[backup_migrate][subdir] = "contrib"
projects[block_class][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[ckeditor][subdir] = "contrib"
projects[devel][subdir] = "contrib"
projects[devel_themer][subdir] = "contrib"
projects[ds][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[eva][subdir] = "contrib"
projects[extlink][subdir] = "contrib"
projects[fast_404][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[field_collection][subdir] = "contrib"
projects[field_delimiter][subdir] = "contrib"
projects[field_formatter_settings][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[field_permissions][subdir] = "contrib"
projects[filter_perms][subdir] = "contrib"
projects[fitvids][subdir] = "contrib"
projects[globalredirect][subdir] = "contrib"
projects[imageblock][subdir] = "contrib"
projects[jquery_update][subdir] = "contrib"
projects[libraries][subdir] = "contrib"
projects[media][subdir] = "contrib"
projects[media_vimeo][subdir] = "contrib"
projects[media_youtube][subdir] = "contrib"
projects[menu_attributes][subdir] = "contrib"
projects[menu_trail_by_path][subdir] = "contrib"
projects[metatag][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[redirect][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[transliteration][subdir] = "contrib"
projects[variable][subdir] = "contrib"
projects[views][subdir] = "contrib"

; MULTILINGUAL
; Content Translation
projects[i18n][subdir] = "contrib"
; Entity Translation
projects[entity_translation][subdir] = "contrib"
projects[title][subdir] = "contrib"
; Recommended
projects[bean][subdir] = "contrib"
projects[lang_dropdown][subdir] = "contrib"

; Themes
projects[] = shiny
projects[] = omega
projects[] = bootstrap

; Libraries
libraries[fitvids][download][type] = "file"
libraries[fitvids][download][url] = "https://raw.githubusercontent.com/davatron5000/FitVids.js/master/jquery.fitvids.js"

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.7/ckeditor_4.4.7_standard.zip"